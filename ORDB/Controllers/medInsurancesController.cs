﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORDB.Models;

namespace ORDB.Controllers
{
    public class medInsurancesController : Controller
    {
        private ordbEntities db = new ordbEntities();

        // GET: medInsurances
        public ActionResult Index()
        {
            return View(db.medInsurances.ToList());
        }

        // GET: medInsurances/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medInsurance medInsurance = db.medInsurances.Find(id);
            if (medInsurance == null)
            {
                return HttpNotFound();
            }
            return View(medInsurance);
        }

        // GET: medInsurances/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: medInsurances/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,name")] medInsurance medInsurance)
        {
            if (ModelState.IsValid)
            {
                db.medInsurances.Add(medInsurance);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(medInsurance);
        }

        // GET: medInsurances/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medInsurance medInsurance = db.medInsurances.Find(id);
            if (medInsurance == null)
            {
                return HttpNotFound();
            }
            return View(medInsurance);
        }

        // POST: medInsurances/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,name")] medInsurance medInsurance)
        {
            if (ModelState.IsValid)
            {
                db.Entry(medInsurance).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(medInsurance);
        }

        // GET: medInsurances/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medInsurance medInsurance = db.medInsurances.Find(id);
            if (medInsurance == null)
            {
                return HttpNotFound();
            }
            return View(medInsurance);
        }

        // POST: medInsurances/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            medInsurance medInsurance = db.medInsurances.Find(id);
            db.medInsurances.Remove(medInsurance);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
