﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORDB.Models;

namespace ORDB.Controllers
{
    public class coveragesController : Controller
    {
        private ordbEntities db = new ordbEntities();

        // GET: coverages
        public ActionResult Index()
        {
            var coverages = db.coverages.Include(c => c.medInsurance);
            return View(coverages.ToList());
        }

        // GET: coverages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            coverage coverage = db.coverages.Find(id);
            if (coverage == null)
            {
                return HttpNotFound();
            }
            return View(coverage);
        }

        // GET: coverages/Create
        public ActionResult Create()
        {
            ViewBag.medInsID = new SelectList(db.medInsurances, "ID", "name");
            return View();
        }

        // POST: coverages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,name,coverage1,medInsID")] coverage coverage)
        {
            if (ModelState.IsValid)
            {
                db.coverages.Add(coverage);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.medInsID = new SelectList(db.medInsurances, "ID", "name", coverage.medInsID);
            return View(coverage);
        }

        // GET: coverages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            coverage coverage = db.coverages.Find(id);
            if (coverage == null)
            {
                return HttpNotFound();
            }
            ViewBag.medInsID = new SelectList(db.medInsurances, "ID", "name", coverage.medInsID);
            return View(coverage);
        }

        // POST: coverages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,name,coverage1,medInsID")] coverage coverage)
        {
            if (ModelState.IsValid)
            {
                db.Entry(coverage).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.medInsID = new SelectList(db.medInsurances, "ID", "name", coverage.medInsID);
            return View(coverage);
        }

        // GET: coverages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            coverage coverage = db.coverages.Find(id);
            if (coverage == null)
            {
                return HttpNotFound();
            }
            return View(coverage);
        }

        // POST: coverages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            coverage coverage = db.coverages.Find(id);
            db.coverages.Remove(coverage);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
