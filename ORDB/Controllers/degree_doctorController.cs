﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORDB.Models;

namespace ORDB.Controllers
{
    public class degree_doctorController : Controller
    {
        private ordbEntities db = new ordbEntities();

        // GET: degree_doctor
        public ActionResult Index()
        {
            var degree_doctor = db.degree_doctor.Include(d => d.degree).Include(d => d.doctor);
            return View(degree_doctor.ToList());
        }

        // GET: degree_doctor/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            degree_doctor degree_doctor = db.degree_doctor.Find(id);
            if (degree_doctor == null)
            {
                return HttpNotFound();
            }
            return View(degree_doctor);
        }

        // GET: degree_doctor/Create
        public ActionResult Create()
        {
            ViewBag.degreeID = new SelectList(db.degrees, "ID", "name");
            ViewBag.doctorID = new SelectList(db.doctors, "ID", "name");
            return View();
        }

        // POST: degree_doctor/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,doctorID,degreeID,specialization,orgFrom")] degree_doctor degree_doctor)
        {
            if (ModelState.IsValid)
            {
                db.degree_doctor.Add(degree_doctor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.degreeID = new SelectList(db.degrees, "ID", "name", degree_doctor.degreeID);
            ViewBag.doctorID = new SelectList(db.doctors, "ID", "name", degree_doctor.doctorID);
            return View(degree_doctor);
        }

        // GET: degree_doctor/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            degree_doctor degree_doctor = db.degree_doctor.Find(id);
            if (degree_doctor == null)
            {
                return HttpNotFound();
            }
            ViewBag.degreeID = new SelectList(db.degrees, "ID", "name", degree_doctor.degreeID);
            ViewBag.doctorID = new SelectList(db.doctors, "ID", "name", degree_doctor.doctorID);
            return View(degree_doctor);
        }

        // POST: degree_doctor/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,doctorID,degreeID,specialization,orgFrom")] degree_doctor degree_doctor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(degree_doctor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.degreeID = new SelectList(db.degrees, "ID", "name", degree_doctor.degreeID);
            ViewBag.doctorID = new SelectList(db.doctors, "ID", "name", degree_doctor.doctorID);
            return View(degree_doctor);
        }

        // GET: degree_doctor/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            degree_doctor degree_doctor = db.degree_doctor.Find(id);
            if (degree_doctor == null)
            {
                return HttpNotFound();
            }
            return View(degree_doctor);
        }

        // POST: degree_doctor/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            degree_doctor degree_doctor = db.degree_doctor.Find(id);
            db.degree_doctor.Remove(degree_doctor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
