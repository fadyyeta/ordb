﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORDB.Models;

namespace ORDB.Controllers
{
    public class prescriptionsController : Controller
    {
        private ordbEntities db = new ordbEntities();

        // GET: prescriptions
        public ActionResult Index()
        {
            var prescriptions = db.prescriptions.Include(p => p.doctor).Include(p => p.patient);
            return View(prescriptions.ToList());
        }

        // GET: prescriptions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            prescription prescription = db.prescriptions.Find(id);
            if (prescription == null)
            {
                return HttpNotFound();
            }
            return View(prescription);
        }

        // GET: prescriptions/Create
        public ActionResult Create()
        {
            ViewBag.docID = new SelectList(db.doctors, "ID", "name");
            ViewBag.patientID = new SelectList(db.patients, "ID", "fName");
            return View();
        }

        // POST: prescriptions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,patientID,date,docID,comments")] prescription prescription)
        {
            if (ModelState.IsValid)
            {
                db.prescriptions.Add(prescription);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.docID = new SelectList(db.doctors, "ID", "name", prescription.docID);
            ViewBag.patientID = new SelectList(db.patients, "ID", "fName", prescription.patientID);
            return View(prescription);
        }

        // GET: prescriptions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            prescription prescription = db.prescriptions.Find(id);
            if (prescription == null)
            {
                return HttpNotFound();
            }
            ViewBag.docID = new SelectList(db.doctors, "ID", "name", prescription.docID);
            ViewBag.patientID = new SelectList(db.patients, "ID", "fName", prescription.patientID);
            return View(prescription);
        }

        // POST: prescriptions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,patientID,date,docID,comments")] prescription prescription)
        {
            if (ModelState.IsValid)
            {
                db.Entry(prescription).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.docID = new SelectList(db.doctors, "ID", "name", prescription.docID);
            ViewBag.patientID = new SelectList(db.patients, "ID", "fName", prescription.patientID);
            return View(prescription);
        }

        // GET: prescriptions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            prescription prescription = db.prescriptions.Find(id);
            if (prescription == null)
            {
                return HttpNotFound();
            }
            return View(prescription);
        }

        // POST: prescriptions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            prescription prescription = db.prescriptions.Find(id);
            db.prescriptions.Remove(prescription);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
