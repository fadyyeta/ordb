﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORDB.Models;

namespace ORDB.Controllers
{
    public class paymentsController : Controller
    {
        private ordbEntities db = new ordbEntities();

        // GET: payments
        public ActionResult Index()
        {
            var payments = db.payments.Include(p => p.Office).Include(p => p.patient).Include(p => p.staff);
            return View(payments.ToList());
        }

        // GET: payments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            payment payment = db.payments.Find(id);
            if (payment == null)
            {
                return HttpNotFound();
            }
            return View(payment);
        }

        // GET: payments/Create
        public ActionResult Create()
        {
            ViewBag.officeID = new SelectList(db.Offices, "ID", "name");
            ViewBag.PatientID = new SelectList(db.patients, "ID", "fName");
            ViewBag.staffID = new SelectList(db.staffs, "ID", "name");
            return View();
        }

        // POST: payments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,PatientID,type,amount,date,officeID,staffID")] payment payment)
        {
            if (ModelState.IsValid)
            {
                db.payments.Add(payment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.officeID = new SelectList(db.Offices, "ID", "name", payment.officeID);
            ViewBag.PatientID = new SelectList(db.patients, "ID", "fName", payment.PatientID);
            ViewBag.staffID = new SelectList(db.staffs, "ID", "name", payment.staffID);
            return View(payment);
        }

        // GET: payments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            payment payment = db.payments.Find(id);
            if (payment == null)
            {
                return HttpNotFound();
            }
            ViewBag.officeID = new SelectList(db.Offices, "ID", "name", payment.officeID);
            ViewBag.PatientID = new SelectList(db.patients, "ID", "fName", payment.PatientID);
            ViewBag.staffID = new SelectList(db.staffs, "ID", "name", payment.staffID);
            return View(payment);
        }

        // POST: payments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,PatientID,type,amount,date,officeID,staffID")] payment payment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(payment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.officeID = new SelectList(db.Offices, "ID", "name", payment.officeID);
            ViewBag.PatientID = new SelectList(db.patients, "ID", "fName", payment.PatientID);
            ViewBag.staffID = new SelectList(db.staffs, "ID", "name", payment.staffID);
            return View(payment);
        }

        // GET: payments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            payment payment = db.payments.Find(id);
            if (payment == null)
            {
                return HttpNotFound();
            }
            return View(payment);
        }

        // POST: payments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            payment payment = db.payments.Find(id);
            db.payments.Remove(payment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
