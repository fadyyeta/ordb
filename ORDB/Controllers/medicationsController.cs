﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ORDB.Models;

namespace ORDB.Controllers
{
    public class medicationsController : Controller
    {
        private ordbEntities db = new ordbEntities();

        // GET: medications
        public ActionResult Index()
        {
            var medications = db.medications.Include(m => m.medicine).Include(m => m.prescription);
            return View(medications.ToList());
        }

        // GET: medications/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medication medication = db.medications.Find(id);
            if (medication == null)
            {
                return HttpNotFound();
            }
            return View(medication);
        }

        // GET: medications/Create
        public ActionResult Create()
        {
            ViewBag.medID = new SelectList(db.medicines, "ID", "name");
            ViewBag.prescriptionID = new SelectList(db.prescriptions, "ID", "comments");
            return View();
        }

        // POST: medications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,prescriptionID,medID,comment")] medication medication)
        {
            if (ModelState.IsValid)
            {
                db.medications.Add(medication);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.medID = new SelectList(db.medicines, "ID", "name", medication.medID);
            ViewBag.prescriptionID = new SelectList(db.prescriptions, "ID", "comments", medication.prescriptionID);
            return View(medication);
        }

        // GET: medications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medication medication = db.medications.Find(id);
            if (medication == null)
            {
                return HttpNotFound();
            }
            ViewBag.medID = new SelectList(db.medicines, "ID", "name", medication.medID);
            ViewBag.prescriptionID = new SelectList(db.prescriptions, "ID", "comments", medication.prescriptionID);
            return View(medication);
        }

        // POST: medications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,prescriptionID,medID,comment")] medication medication)
        {
            if (ModelState.IsValid)
            {
                db.Entry(medication).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.medID = new SelectList(db.medicines, "ID", "name", medication.medID);
            ViewBag.prescriptionID = new SelectList(db.prescriptions, "ID", "comments", medication.prescriptionID);
            return View(medication);
        }

        // GET: medications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medication medication = db.medications.Find(id);
            if (medication == null)
            {
                return HttpNotFound();
            }
            return View(medication);
        }

        // POST: medications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            medication medication = db.medications.Find(id);
            db.medications.Remove(medication);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
