//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ORDB.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class payment
    {
        public int ID { get; set; }
        public Nullable<int> PatientID { get; set; }
        public string type { get; set; }
        public Nullable<decimal> amount { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public Nullable<int> officeID { get; set; }
        public Nullable<int> staffID { get; set; }
    
        public virtual Office Office { get; set; }
        public virtual patient patient { get; set; }
        public virtual staff staff { get; set; }
    }
}
