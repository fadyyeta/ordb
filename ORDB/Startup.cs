﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ORDB.Startup))]
namespace ORDB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
